from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import UserForm, ProfileForm
from django.db import transaction
from django.contrib import messages
from loginsys.forms import RegisterForm

#в приложение аккаунт
@login_required
@transaction.atomic
def update_profile(request, username):
    user = request.user
    if request.POST:
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request,'Ваш профиль был успешно обновлен!')
            return redirect('profile.html', {"user":user})
        else:
            messages.error(request, 'Пожалуйста, исправьте ошибки.')
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'profile.html', {
        'user_form': user_form,
        'profile_form': profile_form,
        'user': user,
    })

