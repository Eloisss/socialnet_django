# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    print('update_user_profile')
    if created:
        print('update_user_1')
        for user in User.objects.all():
            print('update_user_2')
            Profile.objects.get_or_create(user=user)
        print('update_user_3')
        # Profile.objects.create(user=instance)
    print('update_user_4')
    instance.profile.save()
    print('update_user_5')

post_save.connect(update_user_profile, sender=User, dispatch_uid="name_your signal")
#сигналы типа receiver лучше убрать из models.py и создать в этом же приложении отдельный файл signals.py и подключить его в apps.py
